#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    url="https://github.com/python-pillow/Pillow/archive/refs/tags/$version.zip"
    wget -q $url -O src.zip
    unzip -q src.zip
    cd pillow-$version
}

dependencies()
{
    deps_root=$(cygpath -w "$(pwd)")/dependencies
    export VCPKG_DEFAULT_TRIPLET=arm64-windows
    vcpkg.exe install\
        --x-install-root="$deps_root" \
        zlib libwebp lcms freetype libjpeg-turbo libpng tiff

    deps_root="$deps_root/$VCPKG_DEFAULT_TRIPLET"
    LIB="${LIB:-}"
    export LIB="$deps_root/lib;$LIB"
    INCLUDE="${INCLUDE:-}"
    export INCLUDE="$deps_root/include;$LIB"

    # get dll needed for wheel
    deps_root="$(cygpath -u $deps_root)"
    ls $deps_root/bin/*.dll
    cp -r $deps_root/bin/*.dll src/PIL/
    echo 'include src/PIL/*.dll' >> MANIFEST.in
}

build()
{
    git clone -q https://gitlab.com/Linaro/windowsonarm/packagetools

    python \
        ./packagetools/scripts/cross_compile_python_wheel.py \
        --python-version 3.10.4 \
        --source-dir .
}

package()
{
    mv dist/*.whl $out_dir
}

checkout
dependencies
build
package
